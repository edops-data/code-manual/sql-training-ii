# Tag Attendance as Present or Absent

This is the first piece of the 3-piece report.  In this piece, we will tag all attendance as either Present or Absent.

Technically, this is already done in the SIS.  However, we need to convert the words `Present` and `Absent` into `1` and `0`, respectively, so that they're easier to work with.

## Functions You Will Use

-   `SELECT`
-   `CASE`
-   `LEFT JOIN`

You will also employ **Aliasing**.

## Step 1.  `SELECT` some columns

The `SELECT` command tells the database to pull (or select! get it?) a collection of columns for you.

The most basic version of `SELECT` is `SELECT *`, which translates to _Screw it, give me all the columns. I'll figure it out later._  Let's try it!

```sql
SELECT *
FROM Attendance
```

> Notice that we also specify the table we're pulling `FROM`

This will return _every attendance record, ever_ from your server.  It's overkill in every sense, so let's pare it down a bit.

What we really want is the **Student ID**, the **Date**, the **School Year**, and the **Attendance Code** of each record.  We also need to know whether it is **Daily** or **Meeting** attendance, since ultimately we'll need to filter down to just the Daily records.

```sql
SELECT StudentID,
  YearID,
  Att_Date,
  Att_Mode_Code,
  Attendance_CodeID
FROM Attendance
```

> **Style Note:** Columns 2-n get **indented** by one tab stop.

Now we have the information we're after.  But which records are _Present_ and which are not?  A numeric code isn't very useful.  Fortunately, there's a table named `Attendance_Code` that will translate each numeric `Att_Code` into `Present` or `Absent`.

## Step 2.  `JOIN` the Attendance Codes

We attach tables to each other using a `JOIN`.  There are lots of analogies that explain how a join works; here's mine:

> A JOIN gives you all the records from Table B that correspond to a **single record** from Table A.  In order to get the _right_ records (and not just _all_ the records) we have to specify all the necessary information to pare down the search.

Fortunately, this is easy since the `Attendance_CodeID` provided in the `Attendance` table is a **unique identifier**, meaning it's all we need to get the single value we're after.

We'll add the desired column (`Presence_Status_CD`) and join the appropriate table (`Attendance_Code`):

```sql
SELECT StudentID,
  YearID,
  Att_Date,
  Att_Mode_Code,
  Attendance_CodeID,
  Presence_Status_CD
FROM Attendance
LEFT JOIN Attendance_Code ON ID = Attendance_CodeID
```

> **Technical Note:** There are joins other than the `LEFT JOIN`.  But we're only going to use `LEFT JOIN` here.

**This query will fail**.  The reason is that both tables have an `ID` column, so the engine doesn't know which to pull.  To solve this, we need to specify which table each column comes from.  Since writing _Attendance..._ over and over is a nuisance, we're going to **alias** our tables.

## Step 3.  Alias the tables

Aliasing does what it sounds like: it gives a nickname to each table or column that makes it easier to type, or avoids ambiguities.

```sql
SELECT att.StudentID,
  att.YearID,
  att.Att_Date,
  att.Att_Mode_Code,
  att.Attendance_CodeID,
  attc.Presence_Status_CD
FROM Attendance att
LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
```

> **Style Note:** When aliasing a table, just include the alias after the table name.  When aliasing a column, include the word `AS` prior to the alias.  We'll see an example of this shortly.

## Step 4.  Convert _Present/Absent_ to `1/0` using `CASE`

We convert values from one syntax to another using `CASE`.  `CASE` works like a compact set of **if/then** statements (only using the word _WHEN_ instead of _IF_).  The last clause of a `CASE` statement is the `ELSE` clause, which specifies what to do if none of the prior conditions are met.

```sql
SELECT att.StudentID,
  att.YearID,
  att.Att_Date,
  att.Att_Mode_Code,
  att.Attendance_CodeID,
  attc.Presence_Status_CD,
  (CASE
    WHEN attc.Presence_Status_CD = 'Present' THEN 1
    ELSE 0
  END) AS Present
FROM Attendance att
LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
```

Unlike the columns that precede it, the `CASE` statement has no name, so we create one with an **alias** (notice how this one used `AS`).

> **Style Note:** Each clause of the `CASE` statement gets its own, indented line.  The parentheses enclosing the statement are optional, but helpful.

## You now have a functioning query!

Too bad it's not useful yet.  Let's get to that in Part 2.
