# SQL Training II

## Today's Goal:

Create a SQL query from scratch.  Our query will display students who are chronically absent - i.e. have missed at least 10% of school days.

## Part 0: Preparation

You should already have Git and Atom installed, and likely have your school's repository cloned.  However, if you do not, visit [https://gitlab.com/edops-data/code-manual/sql/blob/master/README.md](https://gitlab.com/edops-data/code-manual/sql/blob/master/README.md) and follow Parts 0-2.

## Part 1: Get into APEX

Open the PS Admin portal of your favorite school.

Make a new tab, and navigate to `https://[your PS Instance]:8443/ords`.  For example, `https://paulpcs.powerschool.com:8443/ords`
![](./images/apexlogin.PNG).  Sign in using your APEX credentials, which are different from your PS credentials.

Click on **SQL Workshop**, and then on **SQL Commands**.

## Part 2: Report Overview

Our report is going to do 3 things, in sequence:

1.  Tag each daily attendance entry as _Present_ or _Absent_, using a `1` for Present and a `0` for Absent
2.  Count the number of days enrolled, and the number of days present; and divide them to get the ISA for each student
3.  Filter and sort the list to obtain students with <=90% ISA (i.e. >=10% days missed)

## Report Piece 1: Tag daily attendance as Present or Absent

Details [here](./attendancecoded.md).

### Pre-Built Example

Note that this example also tags Meeting Attendance, but we'll deal with that later.

```sql
SELECT att.StudentID,
  att.YearID,
  att.Att_Date,
  att.Att_Mode_Code,
  (CASE
    WHEN attc.Presence_Status_CD = 'Present' THEN 1
    ELSE 0
  END) AS Present
FROM Attendance att
LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
```

## Report Piece 2: Calculate In-Seat Attendance

Details [here](./isa.md).

### Pre-Built Example

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
SELECT StudentID,
  YearID,
  COUNT(Att_Date) AS DaysEnrolled,
  SUM(Present) AS DaysPresent,
  ROUND(SUM(Present) / COUNT(Att_Date), 2) AS ISA
FROM AttendanceCoded
WHERE Att_Mode_Code = 'ATT_ModeDaily'
GROUP BY StudentID, YearID
```
## Report Piece 3: Filter the results and add student information

Details [here](./chronicabsent.md)

### Pre-Built Example

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM Students s
LEFT JOIN ISA i ON i.StudentID = s.ID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.YearID = 29
  AND i.ISA < 90
  AND s.Enroll_Status = 0
ORDER BY s.Grade_Level ASC, i.ISA DESC, s.LastFirst ASC
```
