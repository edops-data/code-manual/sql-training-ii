# Calculate In-Seat Attendance

This is the second piece of a 3-piece report.  In this piece, we will calculate each student's yearly In-Seat Attendance (ISA) using the formula:

`ISA = [Days Present] / [Days Enrolled] * 100`

## Functions You Will use

-   `COUNT`
-   `SUM`
-   `ROUND`
-   `WHERE`
-   `GROUP BY`

You will also make use of a **common table expression** to import the query from the last piece.

## Step 1.   Create a Common Table Expression

We will be making use of our query from Part 1, but to do so, we need to package it up and give it a name.  Easy enough:

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    att.Attendance_CodeID,
    attc.Presence_Status_CD,
    (CASE
      WHEN attc.Presence_Status_CD = 'Present' THEN 1
      ELSE 0
    END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
```

> **Style Note:** Everything inside parentheses gets indented one added tab stop.

You can't run this query on its own anymore, since nothing's actually being _done_ with our `AttendanceCoded` query.  Time to fix that.

## Step 2.  `COUNT` and `SUM` the days enrolled and present, respectively

For reasons not worth going into, we have to aggregate the _Days Enrolled_ and _Days Present_ in different ways:

-   **Days Enrolled** is a simple count of attendance records
-   **Days Present** is a sum of the `Present` column, which we set to equal `1` when the student is present and `0` otherwise.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
SELECT StudentID,
  YearID,
  COUNT(Att_Date) AS DaysEnrolled,
  SUM(Present) AS DaysPresent
FROM AttendanceCoded
```

> **Technical Note:** Notice how we can refer to our old query as though it were its own table!  The joys of CTEs.

**This query will fail**.  That's because we used two **aggregate functions** (`COUNT` and `SUM`) _without_ telling the engine which columns _not_ to aggregate (you think it would be obvious, but here we are).

## Step 3.  Make the aggregation work with `GROUP BY`

Any columns that are _not_ being aggregated go in a `GROUP BY` clause.  In other words, we're asking for our data to be _grouped_ per student, per year.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
SELECT StudentID,
  YearID,
  COUNT(Att_Date) AS DaysEnrolled,
  SUM(Present) AS DaysPresent
FROM AttendanceCoded
GROUP BY StudentID, YearID
```

> **Technical Note**: The `GROUP BY` clause is always the last or next-to-last clause in a query (followed only by `ORDER BY`, which we'll get to later).

## Step 4.  Perform some division to get the ISA rate

Right now we have some lovely counts of Days Enrolled and Days Present, but we don't yet have an actual rate.  That will require some division and some rounding (pesky fractions!).

The `ROUND()` function works the same in SQL as it does in Excel.  While we're here, we should also multiply our percent by 100 for readability.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
SELECT StudentID,
  YearID,
  COUNT(Att_Date) AS DaysEnrolled,
  SUM(Present) AS DaysPresent,
  ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
FROM AttendanceCoded
GROUP BY StudentID, YearID
```

> **Style Note**: Our style guide declares that there should be a single space on either side of an arithmetic operator.

##  Step 5.  Restrict to just daily attendance

Although our query _looks_ like it's working great, the numbers it's returning are actually garbage.  We're still including all of the meeting attendance records, so this ratio is actually counting **periods enrolled** and **periods present**, as well as days!  Time to fix that.

A `WHERE` clause tells the query what constraints to put on the data.  It comes at the **end** of the query, followed only by `GROUP BY` clauses.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
)
SELECT StudentID,
  YearID,
  COUNT(Att_Date) AS DaysEnrolled,
  SUM(Present) AS DaysPresent,
  ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
FROM AttendanceCoded
WHERE Att_Mode_Code = 'ATT_ModeDaily'
GROUP BY StudentID, YearID
```

> **Technical Note**:  Notice how we don't have any aliases in this new query?  That's because everything is coming from one table, so there's no ambiguity.  Hooray for laziness!  Seriously, though, from a style guide perspective, we should probably alias these columns anyway, just in case the code gets repurposed.

## You now have a functioning query!

In the third and final installment of this exciting journey, we will filter the results.
