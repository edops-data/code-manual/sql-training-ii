# Filter results and attach student information

This is the final piece of a 3-piece report.  In this piece, we will filter the results to include only students who are chronically absent for the current year, and then tack on some identifying information.

## Functions You Will Use

-   `LEFT JOIN`
-   `WHERE`
-   `ORDER BY`

## Step 1.  Turn our query from the last piece into a Common Table Expression

Same as last time, we need to take the code we just created and turn it into a CTE. The only difference is that we don't need to repeat the `WITH` clause, and we need to use a comma to separate the two CTEs.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
```

## Step 2.  Select something from our query to make sure it works

For all the work we've done so far, we're only going to use one column worth of it: the `ISA` column from the `ISA` query.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT i.ISA
FROM ISA i
```

> **Technical Note**: We're going to have multiple tables floating around, so it's prudent to get our aliases established right now.

This may be the most useless query we've run yet, since it just returns a bunch of ISA rates with no other data.  We'll fix that in the next step.

## Step 3.  Pull student information from the `Students` table

We're going to use another `LEFT JOIN` to attach the `Students` table to our query using the `StudentID` field we made sure to pull in each of our subqueries.  For ease of use, we will display the **student name**, **student number**, and **grade level**.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
```

### Step 3b.  Campus Abbreviation

For schools with multiple campuses, it would also be kind of us to give the **campus abbreviation**.  For that, we can join on the `Schools` table in a similar manner:

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
```

> **Technical Note**: Columns can be called in any order, irrespective of when they enter the query.  Joins, however, require a particular order - but that's outside our scope today.

## Step 4.  Filter the data

It's time to filter out everybody who is _not_ chronically absent.  This is the same procedure that we used in the 2nd piece of the query.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.ISA < 90
```

### Step 4b.  Some more filtering

Great!  But you've probably noticed by this step that we're also displaying graduated students, exited students, and other folks who definitely shouldn't be included.  We're also still including past years, which need to go away.  We can fix both with a couple additions to the `WHERE` clause:

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.ISA < 90
  AND i.YearID = 29
  AND s.Enroll_Status = 0
```

> **Technical Note**: `Enroll_Status` is the field that PowerSchool uses to identify active enrollments.  It is `0` when and only when a student is actively enrolled.

> **Style Note**: Lines starting with `AND` are indented by one tab stop.

## Step 5.  Get everything in order

Right now, students appear in the query at random.  Literally, the order will change depending on the day you run the query.  As a courtesy to our end user, we should sort the data, which is done using the `ORDER BY` clause.

The `ORDER BY` clause is the **last clause** in any SQL query.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.ISA < 90
  AND i.YearID = 29
  AND s.Enroll_Status = 0
ORDER BY s.Grade_Level ASC, i.ISA DESC, s.LastFirst ASC
```

## You now have a functioning query!

Go enjoy a lovely beverage.
